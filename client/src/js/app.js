let APP = {};

(function () {

    APP.service = "http://localhost:8080/";

    APP.init = function () {
        APP.createEvents();

    };

    APP.aosInit = function () {
        AOS.init({
            easing: 'ease',
            duration: 1000,
            once: true
        });
    };

    APP.createEvents = function () {

    };

    APP.ajax = function (endpoint, method, content, callback, authRequired) {
        let url = APP.service;

        url += endpoint.join('/');

        let headers = {
            'Access-Control-Allow-Origin': '*'
        };

        if (authRequired) {
            headers.Authorization = 'Bearer ' + account.getAccessToken();
        }

        let data = null;
        if (content !== null) {
            data = content;
            if (method !== 'GET') {
                data = JSON.stringify(data);
            }
        }

        $.ajax({
            headers: headers,
            url: url,
            type: method,
            dataType: "json",
            data: data,
            contentType: "application/json",
            crossDomain: true,
            success: function (data) {
                if (callback) {
                    callback(data);
                }
            },
            error: function (data, textStatus, errorThrown) {
                APP.errorAjaxCallback(data, textStatus, errorThrown);
            }
        });

    };

    APP.errorAjaxCallback = function (data, textStatus, errorThrown) {

        let container = APP.getEBI('containerError');

        APP.removeChildren(container);
        APP.appendChild(
            container,
            APP.createText('Ops.... An error occur while requesting data')
        );

        $(container).fadeIn('fast');
    };

    APP.dispatchEvent = function (obj, evt) {
        let fireOnThis = obj;
        if (document.createEvent) {
            let evObj = fireOnThis.ownerDocument.createEvent('MouseEvents');
            evObj.initEvent(evt, 'yes', 'yes');
            fireOnThis.dispatchEvent(evObj);

        } else if (document.createEventObject) {
            let evObj = document.createEventObject();
            fireOnThis.fireEvent('on' + evt, evObj);
        }
    };


    APP.removeChildren = function (control) {

        if (typeof (control) === "string") {
            control = APP.getEBI(control);
        }

        if (control) {
            while (control.firstChild) {
                control.removeChild(control.firstChild);
            }
        }

    };

    APP.keysCount = function (obj) {
        let keys = [], k;
        for (k in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                keys.push(k);
            }
        }
        return keys.length;
    };


    APP.getAttribute = function (control, attribute) {
        let result = null;
        if (control && attribute !== '') {

            if (control.hasAttribute(attribute)) {
                result = control.getAttribute(attribute);
            }
        }

        return result;
    };

    APP.addEvent = function (control, event, func) {

        if (typeof (control) === 'string') {
            control = APP.getEBI(control);
        }

        if (control) {
            $(control).bind(event, func);
        }
    };

    APP.removeEvent = function (control, event) {
        if (typeof (control) === 'string') {
            control = APP.getEBI(control);
        }

        if (control) {
            $(control).unbind(event);
        }

    };

    APP.getEBI = function (element) {
        return document.getElementById(element);
    };

    APP.show = function (container) {
        $(container).fadeIn('fast');
    };

    APP.hide = function (container) {
        $(container).fadeOut('fast');
    };

    APP.htmlEncode = function (value) {
        return $('<div/>').text(value).html();
    };

    APP.htmlDecode = function (value) {
        return $('<div/>').html(value).text();
    };


    APP.createElement = function (tag) {
        return document.createElement(tag);
    };

    APP.addAttribute = function (element, attributeName, attributeValue) {
        if (element) {
            element.setAttribute(attributeName, attributeValue);
        }

        return element;
    };

    APP.createText = function (value) {
        return document.createTextNode(value);
    };

    APP.appendChild = function (element, child) {
        if (element) {
            element.append(child);
        }

        return element;
    };

    APP.parseUrl = function () {

        let url = new URL(document.URL);

        let path = url.pathname;
        path = path.substr(1);

        return path.split('/');
    };

    APP.getGroupId = function () {

        return APP.parseUrl()[1];
    };

    APP.getProfileId = function () {
        return APP.parseUrl()[1];
    };


})();
