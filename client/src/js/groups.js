let groups = {};

(function () {

    groups.getAll = function () {
        let endpoint = ['group', 'all'];

        APP.ajax(endpoint, 'GET', null, function (result) {

            groups.buildGroupsView(result.data);
            groups.setupFilters();

        });
    };

    groups.buildGroupsView = function (groups) {

        let groupsView = APP.getEBI('portfolio-grid');

        for (let i = 0; i < groups.length; i++) {

            let group = groups[i];


            let groupContainer = APP.createElement('div');
            groupContainer.className = 'item col-sm-6 col-md-4 col-lg-4 mb-4 ' + group.category;

            let groupLink = APP.createElement('a');
            groupLink.href = 'group/' + group.groupId;
            groupLink.className = 'item-wrap fancybox';

            let groupDataContainer = APP.createElement('div');
            groupDataContainer.className = 'work-info';

            let groupName = APP.appendChild(
                APP.createElement('h3'),
                APP.createText(group.groupName)
            );
            groupDataContainer = APP.appendChild(groupDataContainer, groupName);

            let groupCategory = APP.appendChild(
                APP.createElement('span'),
                APP.createText(group.category)
            );
            groupDataContainer = APP.appendChild(groupDataContainer, groupCategory);

            let groupImage = APP.createElement('img');
            groupImage.className = 'img-fluid';
            groupImage.src = group.headerPicture;

            groupLink = APP.appendChild(groupLink, groupDataContainer);
            groupLink = APP.appendChild(groupLink, groupImage);

            groupContainer = APP.appendChild(groupContainer, groupLink);

            groupsView.appendChild(groupContainer);

        }
    };


    groups.isotopeContainer = null;
    groups.setupFilters = function () {

        let $container = $('#portfolio-grid').isotope({
            itemSelector: '.item',
            isFitWidth: true
        });

        groups.isotopeContainer = $container;

        $('#filters').on('click', 'a', function (e) {
            e.preventDefault();
            var filterValue = $(this).attr('data-filter');
            $container.isotope({filter: filterValue});
            $('#filters a').removeClass('active');
            $(this).addClass('active');
        });

        setTimeout(function () {
            groups.isotopeContainer.isotope({filter: '*'});
        }, 1000);

        APP.aosInit();
    };

    groups.loadDetails = function () {

        let groupId = APP.getGroupId();
        let endpoint = ['group', 'page', groupId];

        APP.ajax(endpoint, 'GET', null, function (content) {
            groups.setupDetails(content.data);
        }, true);
    };

    groups.setupDetails = function (content) {


        let btnFollowGroup = APP.getEBI('btnFollowGroup');

        let lblGroupName = APP.getEBI('lblGroupName');
        let lblGroupDescription = APP.getEBI('lblGroupDescription');
        let imgGroupImage = APP.getEBI('imgGroup');
        let lblGroupCategory = APP.getEBI('lblGroupCategory');

        let groupName = APP.createText(content.groupName);
        let groupDescription = APP.createText(content.description);
        let groupCategory = APP.createText(content.category);

        APP.appendChild(lblGroupName, groupName);
        APP.appendChild(lblGroupDescription, groupDescription);
        APP.appendChild(lblGroupCategory, groupCategory);
        imgGroupImage.src = content.headerPicture;

        APP.addAttribute(
            btnFollowGroup,
            'data-id',
            content.groupId
        );

        if (content.followedByUser) {
            groups.setUnfollowButton(btnFollowGroup);
        } else {
            groups.setFollowButton(btnFollowGroup);
        }

        groups.buildMembersView(content.followers);

        APP.aosInit();
    };

    groups.setFollowButton = function (button) {
        APP.removeChildren(
            button
        );

        APP.appendChild(
            button,
            APP.createText('Follow the Group')
        );
        button.className = 'btn btn-primary mb-2';
        $(button).unbind('click');
        $(button).bind('click', function (evt) {
            evt.preventDefault();
            groups.follow(APP.getAttribute(this, 'data-id'));
            return false
        });
    };

    groups.setUnfollowButton = function (button) {

        APP.removeChildren(
            button
        );

        APP.appendChild(
            button,
            APP.createText('Unfollow the Group')
        );

        button.className = 'btn btn-outline-danger mb-2';
        $(button).unbind('click');
        $(button).bind('click', function (evt) {
            evt.preventDefault();
            groups.unfollow(APP.getAttribute(this, 'data-id'));
            return false
        });
    };

    groups.follow = function (groupId) {

        let endpoint = ['group', 'follow'];
        let data = {
            "groupId": groupId
        };

        APP.ajax(endpoint, 'POST', data, groups.followCallback, true);

    };

    groups.followCallback = function (content) {
        groups.setUnfollowButton(
            APP.getEBI('btnFollowGroup')
        );
    };

    groups.unfollow = function (groupId) {
        let endpoint = ['group', 'follow'];
        let data = {
            "groupId": groupId
        };

        APP.ajax(endpoint, 'DELETE', data, groups.unfollowCallback, true);
    };

    groups.unfollowCallback = function (content) {
        groups.setFollowButton(
            APP.getEBI('btnFollowGroup')
        );
    };

    groups.buildMembersView = function (members) {

        let membersView = APP.getEBI('containerMembers');

        for (let i = 0; i < members.length; i++) {

            let member = members[i];

            if (member.accountId === account.getAccountId()) {
                continue;
            }

            let memberContainer = APP.createElement('div');
            memberContainer.className = 'item col-sm-4 col-md-2 col-lg-2 mb-2 ';

            let memberLink = APP.createElement('a');
            memberLink.href = '/profile/' + member.accountId;
            memberLink.className = 'item-wrap fancybox';

            let memberDataContainer = APP.createElement('div');
            memberDataContainer.className = 'work-info';

            let memberName = APP.appendChild(
                APP.createElement('h3'),
                APP.createText(member.name)
            );
            memberDataContainer = APP.appendChild(memberDataContainer, memberName);

            let memberCountry = APP.appendChild(
                APP.createElement('span'),
                APP.createText(member.country)
            );
            memberDataContainer = APP.appendChild(memberDataContainer, memberCountry);

            let memberImage = APP.createElement('img');
            memberImage.className = 'img-fluid';
            memberImage.src = member.profilePicture;
            memberImage.style = 'width:100%';
            memberLink = APP.appendChild(memberLink, memberDataContainer);
            memberLink = APP.appendChild(memberLink, memberImage);

            memberContainer = APP.appendChild(memberContainer, memberLink);

            membersView.appendChild(memberContainer);

        }
    };

})();
