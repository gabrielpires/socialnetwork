let account = {};

(function () {

        account.init = function () {

            account.createEvents();
            account.switchLoginStatus();
        };

        account.guardedPage = function () {

            if (!account.isLogged()) {
                account.addCookie('redirected', true, 1);
                window.location.href = '/';
            }

        };

        account.createEvents = function () {
            $('#btnSignIn').each(function () {
                $(this).bind('click', function (evt) {
                    evt.preventDefault();
                    account.login();
                    return false
                });
            });

            $('#btnSignOut').each(function () {
                $(this).bind('click', function (evt) {
                    evt.preventDefault();
                    account.logout();
                    return false
                });
            });

            $('#btnSearch').bind('click', function (evt) {
                evt.preventDefault();
                account.searchAccounts();
                return false
            });
        };

        account.switchLoginStatus = function () {

            let containerLogin = APP.getEBI('containerLogin');
            let containerProfile = APP.getEBI('containerProfile');

            if (account.isLogged()) {
                account.setupProfile();
                APP.hide(containerLogin);
                APP.show(containerProfile);

            } else {
                APP.show(containerLogin);
                APP.hide(containerProfile);
            }
        };

        account.setupProfile = function () {

            let accountName = APP.getEBI('lblAccountName');
            APP.removeChildren(accountName);
            APP.appendChild(
                accountName,
                APP.createText(
                    account.getAccountName()
                )
            );
            let accountPicture = APP.getEBI('imgAccountPicture');
            accountPicture.src = account.getAccountPicture();

        };

        account.login = function () {

            let data = {
                "login": APP.getEBI('txtLogin').value,
                "password": APP.getEBI('txtPassword').value
            };

            let endpoint = ['account', 'auth'];

            APP.ajax(endpoint, 'POST', data, account.loginCallback);
        };

        account.loginCallback = function (result) {

            let jwt = account.parseJWT(result.data.accessToken);

            let accountData = jwt.data;
            accountData.accessToken = result.data.accessToken;

            account.storageAccountData(accountData);
            account.switchLoginStatus();
            $('#containerAlert').fadeOut('fast');
        };

        account.logout = function () {
            account.addCookie('accountId', null, 0);
            account.addCookie('accountName', null, 0);
            account.addCookie('accountPicture', null, 0);
            account.addCookie('accessToken', null, 0);

            account.guardedPage();
        };

        account.storageAccountData = function (data) {
            account.addCookie('accountId', data.accountId, 1);
            account.addCookie('accountName', data.firstName, 1);
            account.addCookie('accountPicture', data.profilePicture, 1);
            account.addCookie('accessToken', data.accessToken, 1);
        };

        account.isLogged = function () {
            let logged = account.getAccountId() !== null;
            return logged;
        };

        account.getAccessToken = function () {
            return account.getCookie('accessToken');
        };

        account.getAccountId = function () {
            return account.getCookie('accountId');
        };

        account.getAccountName = function () {
            return account.getCookie('accountName');
        };

        account.getAccountPicture = function () {
            return account.getCookie('accountPicture');
        };

        account.loadDetails = function () {

            let endpoint = ['account', 'profile', APP.getProfileId()];

            APP.ajax(endpoint, 'GET', null, function (content) {
                account.setupDetails(content.data);
            }, true);
        };

        account.searchAccounts = function () {
            let data = {
                "groupId": account.getSelectedGroup(),
                "country": account.getSelectedCountry()
            };

            let endpoint = ['account', 'search?'];

            APP.ajax(endpoint, 'GET', data, account.searchAccountsCallback, true);

            APP.aosInit();
        };

        account.searchAccountsCallback = function (content) {

            content = content.data;

            let membersView = APP.getEBI('containerMembers');
            APP.removeChildren(membersView);

            let lblSearchDescription = APP.getEBI('lblSearchDescription');
            APP.removeChildren(lblSearchDescription);
            if (content.items.length === 0) {
                APP.appendChild(
                    lblSearchDescription,
                    APP.createText('Ops, we couldn\'t find techies with the parameters that you selected.')
                );

                return;
            }

            APP.appendChild(
                lblSearchDescription,
                APP.createText('We found ' + content.items.length + ' techies for you.')
            );

            account.fillGroups(content.filtersAvailable.groups, content.filterUsed.group);
            account.fillCountries(content.filtersAvailable.countries, content.filterUsed.country);


            account.buildMembersView(membersView, content.items);
        };

        account.fillCountries = function (countries, currentSelected) {

            let ddlCountries = APP.getEBI('ddlCountries');
            APP.removeChildren(ddlCountries);

            let allOption = APP.createElement('option');
            allOption.value = 'ALL';
            APP.appendChild(
                allOption,
                APP.createText('ALL')
            );
            APP.appendChild(ddlCountries, allOption);

            for (let i = 0; i < countries.length; i++) {
                let country = countries[i];
                let option = APP.createElement('option');
                option.value = country.country;
                if (currentSelected === country.country) {
                    APP.addAttribute(option, 'selected', 'selected');
                }
                APP.appendChild(
                    option,
                    APP.createText(country.country)
                );

                APP.appendChild(
                    ddlCountries,
                    option
                );
            }
        };

        account.fillGroups = function (groups, currentSelected) {

            let ddlGroups = APP.getEBI('ddlGroups');
            APP.removeChildren(ddlGroups);

            let allOption = APP.createElement('option');
            allOption.value = 'ALL';
            APP.appendChild(
                allOption,
                APP.createText('ALL')
            );
            APP.appendChild(ddlGroups, allOption);

            for (let i = 0; i < groups.length; i++) {
                let group = groups[i];
                let option = APP.createElement('option');
                option.value = group.groupId;
                if (currentSelected === group.groupId) {
                    APP.addAttribute(option, 'selected', 'selected');
                }
                APP.appendChild(
                    option,
                    APP.createText(group.groupName)
                );

                APP.appendChild(
                    ddlGroups,
                    option
                );
            }
        };

        account.getSelectedCountry = function () {
            return account.getSelectedValue('ddlCountries');
        };

        account.getSelectedGroup = function () {
            return account.getSelectedValue('ddlGroups');
        };

        account.getSelectedValue = function (elementId) {
            let element = APP.getEBI(elementId);
            let value = element.value;

            return (value === 'ALL') ? null : value;
        };

        account.setupDetails = function (content) {


            let btnFollowProfile = APP.getEBI('btnFollowProfile');

            let lblProfileName = APP.getEBI('lblProfileName');
            let imgProfilePicture = APP.getEBI('imgProfilePicture');
            let lblProfileCountry = APP.getEBI('lblProfileCountry');

            let lblFollowing = APP.getEBI('lblFollowing');
            let lblFollowedBy = APP.getEBI('lblFollowedBy');
            let lblGroups = APP.getEBI('lblGroups');

            APP.appendChild(
                lblProfileName,
                APP.createText(
                    content.firstName + ' ' + content.lastName)
            );

            APP.appendChild(
                lblProfileCountry,
                APP.createText(content.country)
            );

            let countFollowing = 0;
            if (content.following !== null) {
                countFollowing = content.following.length;
            }
            APP.appendChild(
                lblFollowing,
                APP.createText(countFollowing)
            );

            let countFollowers = 0;
            if (content.followers !== null) {
                countFollowers = content.followers.length;
            }
            APP.appendChild(
                lblFollowedBy,
                APP.createText(
                    countFollowers
                )
            );

            let countGroups = 0;
            if (content.groups !== null) {
                countGroups = content.groups.length;
            }
            APP.appendChild(
                lblGroups,
                APP.createText(
                    countGroups
                )
            );
            imgProfilePicture.src = content.profilePicture;

            APP.addAttribute(
                btnFollowProfile,
                'data-id',
                content.accountId
            );

            if (content.friendship) {
                account.setUnfriendButton(btnFollowProfile);
            } else if (content.followedByUser) {
                account.setFollowBackButton(btnFollowProfile);
            } else if (content.followingUser) {
                account.setUnfollowButton(btnFollowProfile);
            } else {
                account.setFollowButton(btnFollowProfile);
            }

            account.buildGroupsView(content.groups);

            APP.aosInit();
        };

        account.setUnfriendButton = function (button) {
            APP.removeChildren(
                button
            );

            APP.appendChild(
                button,
                APP.createText('Unfriend this techie')
            );
            button.className = 'btn btn-outline-info mb-2';
            $(button).unbind('click');
            $(button).bind('click', function (evt) {
                evt.preventDefault();
                account.unfollow(APP.getAttribute(this, 'data-id'));
                return false
            });
        };

        account.setFollowBackButton = function (button) {
            APP.removeChildren(
                button
            );

            APP.appendChild(
                button,
                APP.createText('Follow back')
            );
            button.className = 'btn btn-outline-success mb-2';
            $(button).unbind('click');
            $(button).bind('click', function (evt) {
                evt.preventDefault();
                account.follow(APP.getAttribute(this, 'data-id'));
                return false
            });
        };

        account.setUnfollowButton = function (button) {

            APP.removeChildren(
                button
            );

            APP.appendChild(
                button,
                APP.createText('Unfollow this techie.')
            );

            button.className = 'btn btn-outline-danger mb-2';
            $(button).unbind('click');
            $(button).bind('click', function (evt) {
                evt.preventDefault();
                account.unfollow(APP.getAttribute(this, 'data-id'));
                return false
            });
        };

        account.setFollowButton = function (button) {

            APP.removeChildren(
                button
            );

            APP.appendChild(
                button,
                APP.createText('Follow this techie.')
            );

            button.className = 'btn btn-primary mb-2';
            $(button).unbind('click');
            $(button).bind('click', function (evt) {
                evt.preventDefault();
                account.follow(APP.getAttribute(this, 'data-id'));
                return false
            });
        };

        account.follow = function (accountId) {

            let endpoint = ['account', 'follow'];
            let data = {
                "followingId": accountId
            };

            APP.ajax(endpoint, 'POST', data, account.followCallback, true);

        };

        account.followCallback = function (content) {
            let button = APP.getEBI('btnFollowProfile');
            if (content.data.isFollowingBack) {
                account.setUnfriendButton(
                    button
                );
            } else {
                account.setUnfollowButton(
                    button
                );
            }
        };

        account.unfollow = function (accountId) {
            let endpoint = ['account', 'follow'];
            let data = {
                "followingId": accountId
            };

            APP.ajax(endpoint, 'DELETE', data, account.unfollowCallback, true);
        };


        account.buildMembersView = function (membersView, members) {


            for (let i = 0; i < members.length; i++) {

                let member = members[i];

                if (member.accountId === account.getAccountId()) {
                    continue;
                }

                let memberContainer = APP.createElement('div');
                memberContainer.className = 'item col-sm-4 col-md-2 col-lg-2 mb-2 ';

                let memberLink = APP.createElement('a');
                memberLink.href = '/profile/' + member.accountId;
                memberLink.className = 'item-wrap fancybox';

                let memberDataContainer = APP.createElement('div');
                memberDataContainer.className = 'work-info';

                let memberName = APP.appendChild(
                    APP.createElement('h3'),
                    APP.createText(member.firstName + ' ' + member.lastName)
                );
                memberDataContainer = APP.appendChild(memberDataContainer, memberName);

                let memberCountry = APP.appendChild(
                    APP.createElement('span'),
                    APP.createText(member.country)
                );
                memberDataContainer = APP.appendChild(memberDataContainer, memberCountry);

                let memberImage = APP.createElement('img');
                memberImage.className = 'img-fluid';
                memberImage.src = member.profilePicture;
                memberImage.style = 'width:100%';
                memberLink = APP.appendChild(memberLink, memberDataContainer);
                memberLink = APP.appendChild(memberLink, memberImage);

                memberContainer = APP.appendChild(memberContainer, memberLink);

                membersView.appendChild(memberContainer);

            }
        };

        account.buildGroupsView = function (groups) {

            let groupsView = APP.getEBI('portfolio-grid');

            for (let i = 0; i < groups.length; i++) {

                let group = groups[i];

                let groupContainer = APP.createElement('div');
                groupContainer.className = 'item col-sm-6 col-md-4 col-lg-4 mb-4 ' + group.category;

                let groupLink = APP.createElement('a');
                groupLink.href = '/group/' + group.groupId;
                groupLink.className = 'item-wrap fancybox';

                let groupDataContainer = APP.createElement('div');
                groupDataContainer.className = 'work-info';

                let groupName = APP.appendChild(
                    APP.createElement('h3'),
                    APP.createText(group.groupName)
                );
                groupDataContainer = APP.appendChild(groupDataContainer, groupName);

                let groupCategory = APP.appendChild(
                    APP.createElement('span'),
                    APP.createText(group.category)
                );
                groupDataContainer = APP.appendChild(groupDataContainer, groupCategory);

                let groupImage = APP.createElement('img');
                groupImage.className = 'img-fluid';
                groupImage.src = group.headerPicture;

                groupLink = APP.appendChild(groupLink, groupDataContainer);
                groupLink = APP.appendChild(groupLink, groupImage);

                groupContainer = APP.appendChild(groupContainer, groupLink);

                groupsView.appendChild(groupContainer);

            }
        };

        account.unfollowCallback = function (content) {
            let button = APP.getEBI('btnFollowProfile');
            if (content.data.isFollowingBack) {
                account.setFollowBackButton(
                    button
                );
            } else {
                account.setFollowButton(
                    button
                );
            }
        };

        account.addCookie = function (cookieName, value, exdays) {
            let exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);
            let cookieValue = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = cookieName + "=" + cookieValue + ';path=/';
        };

        account.deleteCookie = function (cookieName) {
            account.addCookie(cookieName, '', 0);
        };

        account.getCookie = function (cookieName) {
            let cookieValue = document.cookie;
            let cookieStart = cookieValue.indexOf(" " + cookieName + "=");
            if (cookieStart === -1) {
                cookieStart = cookieValue.indexOf(cookieName + "=");
            }
            if (cookieStart === -1) {
                cookieValue = null;
            } else {
                cookieStart = cookieValue.indexOf("=", cookieStart) + 1;
                let cookieEnd = cookieValue.indexOf(";", cookieStart);
                if (cookieEnd === -1) {
                    cookieEnd = cookieValue.length;
                }
                cookieValue = unescape(cookieValue.substring(cookieStart, cookieEnd));
            }
            return cookieValue;
        };

        account.parseJWT = function (token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

            return JSON.parse(jsonPayload);
        };

        account.alertToLogin = function () {
            let redirected = account.getCookie('redirected');

            if (redirected !== null) {
                $('#containerAlert').fadeIn('fast');
            }
            account.deleteCookie('redirected');
            setTimeout(function () {
                $('#containerAlert').fadeOut('fast');
            }, 180000);
        };

    }
)();
