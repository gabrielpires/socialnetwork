#!/bin/bash

ENV_FILE=./api/.env
ENV_FILE_DIST=./api/.env.dist
if test ! -f "$ENV_FILE"; then
  echo "$ENV_FILE not exist"
  echo "Copying from $ENV_FILE_DIST"
  cp "$ENV_FILE_DIST" "$ENV_FILE"
fi
