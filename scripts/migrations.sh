#!/bin/bash

docker exec -it api-php bash -c 'cd /var/www/ && ./vendor/bin/doctrine-migrations migrate -n'
