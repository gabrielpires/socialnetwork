<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191222143341 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Add created_at to account_groups table';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("ALTER TABLE account_groups ADD COLUMN `date_created` datetime NOT NULL DEFAULT current_timestamp();");

	}

	public function down(Schema $schema): void
	{
		$this->addSql("ALTER TABLE account_groups DROP COLUMN `date_created`;");

	}
}
