<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191222015755 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Create Table Account Token';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("CREATE TABLE `account_token` (
 `account_token_id` varchar(36) NOT NULL,
  `account_id` varchar(36) NOT NULL DEFAULT '',
  `token` text NOT NULL ,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_revoked` datetime DEFAULT NULL,
  PRIMARY KEY (`account_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	}

	public function down(Schema $schema): void
	{
		$this->addSql("DROP TABLE `account_token`;");

	}
}
