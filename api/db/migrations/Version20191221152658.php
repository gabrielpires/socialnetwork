<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221152658 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Create Account Table';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("CREATE TABLE `account` (
  `account_id` varchar(36) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `profile_picture` varchar(500) NOT NULL DEFAULT '',
   `bio` text NOT NULL,
  `birth_date` date NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(500) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_revoked` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	public function down(Schema $schema): void
	{
		$this->addSql("DROP TABLE `account`;");
	}
}
