<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191222110350 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Create Table Following and Drop Table Friendship';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("CREATE TABLE `account_following` (
  `account_id` varchar(36) NOT NULL DEFAULT '',
  `following_id` varchar(36) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  UNIQUE KEY `account_following_id` (`account_id`,`following_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$this->addSql("DROP TABLE `friendship`;");

	}

	public function down(Schema $schema): void
	{
		$this->addSql("DROP TABLE `account_following`;");
		$this->addSql("CREATE TABLE `friendship` (
  `left_account_id` varchar(36) NOT NULL DEFAULT '',
  `right_account_id` varchar(36) NOT NULL DEFAULT '',
  UNIQUE KEY `left_account_id` (`left_account_id`,`right_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	}
}
