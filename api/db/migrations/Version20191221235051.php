<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221235051 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Add unique constraint for account.login';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("ALTER TABLE account ADD CONSTRAINT account_login UNIQUE (login);");

	}

	public function down(Schema $schema): void
	{
		$this->addSql("ALTER TABLE account DROP CONSTRAINT account_login;");

	}
}
