<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221152849 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Create Group Table';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("CREATE TABLE `group` (
  `group_id` varchar(36) NOT NULL DEFAULT '',
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `profile_picture` varchar(500) NOT NULL DEFAULT '',
   `header_picture` varchar(500) NOT NULL DEFAULT '',
   `category` varchar(500) NOT NULL DEFAULT '',
   `description` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_revoked` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	public function down(Schema $schema): void
	{
		$this->addSql("DROP TABLE `group`;");
	}
}
