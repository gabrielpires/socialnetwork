<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221153227 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Create Account Groups Table';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("CREATE TABLE `account_groups` (
  `account_id` varchar(36) NOT NULL DEFAULT '',
  `group_id` varchar(36) NOT NULL DEFAULT '',
  UNIQUE KEY `account_group_id` (`account_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	public function down(Schema $schema): void
	{
		$this->addSql("DROP TABLE `account_groups`;");
	}
}
