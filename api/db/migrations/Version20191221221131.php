<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221221131 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Alter Account Table. Add new columns: login, gender, phone, country';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("ALTER TABLE account ADD COLUMN `login` VARCHAR(50) NOT NULL;");
		$this->addSql("ALTER TABLE account ADD COLUMN `gender` VARCHAR(10) NOT NULL;");
		$this->addSql("ALTER TABLE account ADD COLUMN `phone` VARCHAR(50) NOT NULL;");
		$this->addSql("ALTER TABLE account ADD COLUMN `country` VARCHAR(50) NOT NULL;");
		$this->addSql("ALTER TABLE account ADD COLUMN `country_code` VARCHAR(2) NOT NULL;");
		$this->addSql("ALTER TABLE account ADD COLUMN `city` VARCHAR(50) NOT NULL;");
	}

	public function down(Schema $schema): void
	{
		$this->addSql("ALTER TABLE account DROP COLUMN `login`;");
		$this->addSql("ALTER TABLE account DROP COLUMN `gender`;");
		$this->addSql("ALTER TABLE account DROP COLUMN `phone`;");
		$this->addSql("ALTER TABLE account DROP COLUMN `country`;");
		$this->addSql("ALTER TABLE account ADD COLUMN `country_code` VARCHAR(2) NOT NULL;");
		$this->addSql("ALTER TABLE account DROP COLUMN `city`;");

	}
}
