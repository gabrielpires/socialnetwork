<?php

declare(strict_types=1);

namespace SocialNetwork;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191221154702 extends AbstractMigration
{
	public function getDescription(): string
	{
		return 'Create Friendship Table';
	}

	public function up(Schema $schema): void
	{
		$this->addSql("CREATE TABLE `friendship` (
  `left_account_id` varchar(36) NOT NULL DEFAULT '',
  `right_account_id` varchar(36) NOT NULL DEFAULT '',
  UNIQUE KEY `left_account_id` (`left_account_id`,`right_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

	public function down(Schema $schema): void
	{
		$this->addSql("DROP TABLE `friendship`;");
	}
}
