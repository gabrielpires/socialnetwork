<?php

declare(strict_types=1);

return [
	'name' => 'Social Network Migrations',
	'migrations_namespace' => 'SocialNetwork',
	'table_name' => 'migration_versions',
	'column_name' => 'version',
	'column_length' => 14,
	'executed_at_column_name' => 'executed_at',
	'migrations_directory' => __DIR__ . '/db/migrations',
	'all_or_nothing' => false,
];
