<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

define('APP_ROOT', __DIR__ . '/../');

// Load .env settings
if (is_readable(APP_ROOT . '.env') === false) {
	// if .env file is not present inside the root of our application dir or if the file is not readable, show an error
	header('Content-Type: text/plain');
	echo 'Could not start: please check configuration' . PHP_EOL . 'Error code: 171' . PHP_EOL;
	exit;
}

$dotenv = Dotenv\Dotenv::createImmutable(APP_ROOT);
$dotenv->load();

try {
	$dotenv->required([
		'APPLICATION_NAME',
		'ENVIRONMENT',

		'DB_HOST',
		'DB_NAME',
		'DB_USER',
		'DB_PASS',

		'JWT_KEY',
		'JWT_DURATION'

	]);
} catch (Exception $e) {
	echo 'Could not start: missing configuration setting please check configuration' . PHP_EOL . 'Error code: 172' . PHP_EOL;
	echo $e->getMessage() . PHP_EOL;

	exit;
}

define('APPLICATION_NAME', $_ENV['APPLICATION_NAME']);
define('ENVIRONMENT', $_ENV['ENVIRONMENT']);

define('DB_HOST', $_ENV['DB_HOST']);
define('DB_NAME', $_ENV['DB_NAME']);
define('DB_USER', $_ENV['DB_USER']);
define('DB_PASS', $_ENV['DB_PASS']);

define('JWT_KEY', $_ENV['JWT_KEY']);
define('JWT_DURATION', $_ENV['JWT_DURATION']);



