<?php

declare(strict_types=1);

use SocialNetwork\Controllers\AccountController;
use SocialNetwork\Controllers\GroupController;
use SocialNetwork\Middleware\AuthMiddleware;
use SocialNetwork\Projections\Finder;
use SocialNetwork\Projections\Projector;
use Symfony\Component\DependencyInjection\Reference;

/****************************************************************/
/* PROJECTIONS
/****************************************************************/
$containerBuilder
	->register('app.projections.finder', Finder::class)
	->setArguments([
			new Reference('app.mysql.connection'),
			new Reference('app.infrastructure.logger')
		]
	);

$containerBuilder
	->register('app.projections.projector', Projector::class)
	->setArguments([
			new Reference('app.mysql.connection'),
			new Reference('app.infrastructure.logger')
		]
	);

/****************************************************************/
/* CONTROLLERS
/****************************************************************/
$containerBuilder
	->register('app.controllers.account', AccountController::class)
	->setArguments(
		[
			new Reference('app.projections.projector'),
			new Reference('app.projections.finder'),
			new Reference('app.infrastructure.logger')
		]
	);

$containerBuilder
	->register('app.controllers.group', GroupController::class)
	->setArguments(
		[
			new Reference('app.projections.projector'),
			new Reference('app.projections.finder'),
			new Reference('app.infrastructure.logger')
		]
	);


/****************************************************************/
/* EXTRAS
/****************************************************************/
$containerBuilder
	->register('app.middleware.auth', AuthMiddleware::class)
	->setArguments(
		[
			new Reference('app.projections.finder'),
			new Reference('app.infrastructure.logger')
		]
	);
