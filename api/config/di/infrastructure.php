<?php

use Doctrine\DBAL\DriverManager;
use Monolog\Handler\FilterHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\Definition;

try {
	$stderrHandler = new StreamHandler('php://stderr', Logger::ERROR);
	$stdoutHandler = new FilterHandler(
		new StreamHandler('php://stdout', Logger::DEBUG),
		[Logger::DEBUG, Logger::INFO, Logger::NOTICE, Logger::WARNING]
	);

	$containerBuilder
		->setDefinition('app.infrastructure.logger', new Definition(
			Logger::class,
			[
				'api',
				[
					$stdoutHandler,
					$stderrHandler,
				],
			]
		));
} catch (Throwable $e) {
	exit($e->getMessage());
}


try {
	$connection = DriverManager::getConnection([
		'host' => DB_HOST,
		'dbname' => DB_NAME,
		'user' => DB_USER,
		'password' => DB_PASS,
		'driver' => 'pdo_mysql',
		'charset' => 'utf8',
	]);
	$containerBuilder->set('app.mysql.connection', $connection);
} catch (Throwable $e) {
	echo 'Could not start: Doctrine could not be setup for service database' . PHP_EOL;
	echo 'Code 34 ' . $e->getMessage() . PHP_EOL;

	exit;
}


