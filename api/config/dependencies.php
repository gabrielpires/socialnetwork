<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\ContainerBuilder;

$containerBuilder = new ContainerBuilder();

require __DIR__ . '/di/infrastructure.php';
require __DIR__ . '/di/api.php';

return $containerBuilder;
