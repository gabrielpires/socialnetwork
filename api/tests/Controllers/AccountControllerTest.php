<?php

declare(strict_types=1);
require __DIR__ . '/../../config/config.php';

$container = require APP_ROOT . '/config/dependencies.php';
$GLOBALS['accountController'] = $container->get('app.controllers.account');

use PHPUnit\Framework\TestCase;
use SocialNetwork\Controllers\AccountController;


final class AccountControllerTest extends TestCase
{
	private $rightUserData = [
		'accountId' => '09eef1fb-b845-4540-97cf-71007e759612',
		'login' => 'account.demo',
		'password' => '123456',
		'toFollowAccountId' => '0cb3d8d6-9f92-4ee5-9bf9-0cb438e69e63'
	];

	private $wrongUserData = [
		'accountId' => 'NONE',
		'login' => 'NONE',
		'password' => 'NONE',
		'toFollowAccountId' => 'NONE'
	];

	public function testValidLogin(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$result = $accountController->login(
			$this->rightUserData
		);

		$this->assertIsString($result['data']['accessToken']);
	}

	/**
	 * @expectedException League\Route\Http\Exception\UnauthorizedException
	 */
	public function testInvalidLogin(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$accountController->login(
			$this->wrongUserData
		);

	}

	public function testGetValidProfile(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$result = $accountController->getProfile(
			$this->rightUserData['accountId']
		);

		$this->assertEquals(
			$this->rightUserData['login'],
			$result['data']['login']
		);

	}

	/**
	 * @expectedException SocialNetwork\Models\Account\Exceptions\AccountDoesNotExistException
	 */
	public function testGetInvalidProfile(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$accountController->getProfile(
			$this->wrongUserData['accountId']
		);
	}

	public function testFollowValidAccount(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$result = $accountController->followAccount(
			$this->rightUserData['accountId'],
			$this->rightUserData['toFollowAccountId']
		);

		$this->assertEquals(
			$this->rightUserData['toFollowAccountId'],
			$result['data']['followingId']
		);

	}

	/**
	 * @expectedException \SocialNetwork\Models\Account\Exceptions\AccountAlreadyFollowingAccountException
	 */
	public function testDoubleFollowValidAccount(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$accountController->followAccount(
			$this->rightUserData['accountId'],
			$this->rightUserData['toFollowAccountId']
		);

	}

	public function testUnfollowValidAccount(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$result = $accountController->unfollowAccount(
			$this->rightUserData['accountId'],
			$this->rightUserData['toFollowAccountId']
		);

		$this->assertEquals(
			$this->rightUserData['toFollowAccountId'],
			$result['data']['followingId']
		);
	}

	/**
	 * @expectedException  \SocialNetwork\Models\Account\Exceptions\AccountDoesNotFollowAccountException
	 */
	public function testDoubleUnfollowValidAccount(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$accountController->unfollowAccount(
			$this->rightUserData['accountId'],
			$this->rightUserData['toFollowAccountId']
		);

	}

	/**
	 * @expectedException \SocialNetwork\Models\Account\Exceptions\AccountDoesNotExistException
	 */
	public function testFollowInvalidAccount(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$accountController->followAccount(
			$this->wrongUserData['accountId'],
			$this->wrongUserData['toFollowAccountId']
		);

	}

	/**
	 * @expectedException \SocialNetwork\Models\Account\Exceptions\AccountDoesNotExistException
	 */
	public function testUnfollowInvalidAccount(): void
	{
		/** @var AccountController $accountController */
		$accountController = $GLOBALS['accountController'];

		$accountController->unfollowAccount(
			$this->wrongUserData['accountId'],
			$this->wrongUserData['toFollowAccountId']
		);

	}
}
