<?php

declare(strict_types=1);
require __DIR__ . '/../../config/config.php';

$container = require APP_ROOT . '/config/dependencies.php';
$GLOBALS['groupController'] = $container->get('app.controllers.group');

use PHPUnit\Framework\TestCase;
use SocialNetwork\Controllers\AccountController;
use SocialNetwork\Controllers\GroupController;


final class GroupControllerTest extends TestCase
{
	private $rightUserData = [
		'accountId' => '09eef1fb-b845-4540-97cf-71007e759612',
		'login' => 'account.demo',
		'password' => '123456',
		'groupId' => 'a2c6e770-ad80-456b-b523-ccad933b868e',
		'groupName' => 'PHP Rotterdam'
	];

	private $wrongUserData = [
		'accountId' => 'NONE',
		'login' => 'NONE',
		'password' => 'NONE',
		'groupId' => 'NONE'
	];

	public function testGetValidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$result = $groupController->getPage(
			$this->rightUserData['groupId']
		);

		$this->assertEquals(
			$this->rightUserData['groupName'],
			$result['data']['groupName']
		);

	}

	/**
	 * @expectedException \SocialNetwork\Models\Group\Exceptions\GroupDoesNotExistException
	 */
	public function testGetInvalidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$groupController->getPage(
			$this->wrongUserData['groupId']
		);
	}

	public function testFollowValidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$result = $groupController->followGroup(
			$this->rightUserData['accountId'],
			$this->rightUserData['groupId']
		);

		$this->assertEquals(
			$this->rightUserData['groupId'],
			$result['data']['groupId']
		);
	}

	/**
	 * @expectedException SocialNetwork\Models\Account\Exceptions\AccountAlreadyFollowingGroupException
	 */
	public function testDoubleFollowValidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$groupController->followGroup(
			$this->rightUserData['accountId'],
			$this->rightUserData['groupId']
		);
	}

	public function testUnfollowValidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$result = $groupController->unfollowGroup(
			$this->rightUserData['accountId'],
			$this->rightUserData['groupId']
		);

		$this->assertEquals(
			$this->rightUserData['groupId'],
			$result['data']['groupId']
		);
	}

	/**
	 * @expectedException SocialNetwork\Models\Account\Exceptions\AccountDoesNotFollowGroupException
	 */
	public function testDoubleUnfollowValidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$groupController->unfollowGroup(
			$this->rightUserData['accountId'],
			$this->rightUserData['groupId']
		);

	}

	/**
	 * @expectedException  SocialNetwork\Models\Group\Exceptions\GroupDoesNotExistException
	 */
	public function testFollowInvalidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$groupController->followGroup(
			$this->wrongUserData['accountId'],
			$this->wrongUserData['groupId']
		);

	}

	/**
	 * @expectedException  SocialNetwork\Models\Group\Exceptions\GroupDoesNotExistException
	 */
	public function testUnfollowInvalidGroup(): void
	{
		/** @var GroupController $groupController */
		$groupController = $GLOBALS['groupController'];

		$groupController->unfollowGroup(
			$this->wrongUserData['accountId'],
			$this->wrongUserData['groupId']
		);
	}

}
