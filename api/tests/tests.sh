#!/bin/bash

cd /var/www
./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests/Controllers/AccountControllerTest
./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests/Controllers/GroupControllerTest
