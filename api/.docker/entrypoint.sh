#!/bin/bash

cd /var/www/ && ./vendor/bin/doctrine-migrations migrate -n

set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"
