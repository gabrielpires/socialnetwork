<?php

declare(strict_types=1);

namespace SocialNetwork\Models;


use DateTimeImmutable;
use Exception;
use Ramsey\Uuid\Uuid;

final class Group
{
	/**
	 * @var string
	 */
	private $groupId;

	/**
	 * @var string
	 */
	private $groupName;

	/**
	 * @var string
	 */
	private $profilePicture;

	/**
	 * @var string
	 */
	private $headerPicture;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var string
	 */
	private $category;

	/**
	 * @var DateTimeImmutable
	 */
	private $dateCreated;

	/**
	 * @var DateTimeImmutable
	 */
	private $dateUpdated;

	/**
	 * @var DateTimeImmutable|null
	 */
	private $dateRevoked;

	/**
	 * @var Account[]|null
	 */
	private $followers;

	/**
	 * @var bool
	 */
	private $followedByUser;

	/**
	 * Group constructor.
	 *
	 * @param array|null $payload
	 *
	 * @throws Exception
	 */
	public function __construct(array $payload = null)
	{
		if ($payload !== null) {
			$this->fromPayload($payload);
		}
	}

	/**
	 * @param array $payload
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function fromPayload(array $payload): self
	{
		$this->groupId = $payload['groupId'] ?? Uuid::uuid4()->toString();
		$this->groupName = $payload['groupName'];
		$this->profilePicture = $payload['profilePicture'];
		$this->headerPicture = $payload['headerPicture'];
		$this->description = $payload['description'];
		$this->category = $payload['category'];
		$this->dateCreated = $payload['dateCreated'];
		$this->dateUpdated = $payload['dateUpdated'];
		$this->dateRevoked = $payload['dateRevoked'] ?? null;
		$this->followedByUser = $payload['followedByUser'] ?? false;

		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray(): array
	{
		$data = [
			'groupId' => $this->groupId,
			'groupName' => $this->groupName,
			'profilePicture' => $this->profilePicture,
			'headerPicture' => $this->headerPicture,
			'description' => $this->description,
			'category' => $this->category,
			'dateCreated' => $this->dateCreated,
			'dateUpdated' => $this->dateUpdated,
			'dateRevoked' => $this->dateRevoked,
			'followers' => null,
			'followedByUser' => $this->followedByUser
		];

		if (!empty($this->followers)) {
			$followers = [];

			foreach ($this->followers as $follower) {
				$followers[] = $follower->toDisplayShortAccount();
			}

			$data['followers'] = $followers;
		}

		return $data;
	}

	/**
	 * @param array|null $followers
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function withFollowers(array $followers = null): self
	{
		$clone = clone $this;

		if ($followers !== null) {

			if ($this->followers === null) {
				$this->followers = [];
			}

			foreach ($followers as $follower) {
				$clone->followers[] = Account::createFromPayload($follower);
			}

		}

		return $clone;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function groupId(): string
	{
		if ($this->groupId === null) {
			$this->groupId = Uuid::uuid4()->toString();
		}

		return $this->groupId;
	}

	/**
	 * @return string
	 */
	public function groupName(): string
	{
		return $this->groupName;
	}

	/**
	 * @return string
	 */
	public function profilePicture(): string
	{
		return $this->profilePicture;
	}

	/**
	 * @return string
	 */
	public function headerPicture(): string
	{
		return $this->headerPicture;
	}

	/**
	 * @return string
	 */
	public function description(): string
	{
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function category(): string
	{
		return $this->category;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function dateCreated(): DateTimeImmutable
	{
		return $this->dateCreated;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function dateUpdated(): DateTimeImmutable
	{
		return $this->dateUpdated;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function dateRevoked(): ?DateTimeImmutable
	{
		return $this->dateRevoked;
	}

	/**
	 * @return array|null
	 */
	public function followers(): ?array
	{
		return $this->followers;
	}

	/**
	 * @return bool
	 */
	public function followedByUser(): bool
	{
		return $this->followedByUser;
	}

	/**
	 * STATIC FUNCTIONS
	 */

	/**
	 * @param array $payload
	 *
	 * @return static
	 * @throws Exception
	 */
	public static function createFromPayload(array $payload): self
	{
		return new self($payload);
	}
}
