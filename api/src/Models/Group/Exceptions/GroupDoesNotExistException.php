<?php

declare(strict_types=1);

namespace SocialNetwork\Models\Group\Exceptions;


use Exception;

final class GroupDoesNotExistException extends Exception
{
	/**
	 * @param string $groupId
	 *
	 * @return static
	 */
	public static function withGroupId(string $groupId): self
	{
		$message = 'Group Id: ' . $groupId . ' does not exist';
		return new self($message);
	}
}
