<?php

declare(strict_types=1);

namespace SocialNetwork\Models\Account\Exceptions;


use Exception;

final class AccountDoesNotExistException extends Exception
{
	/**
	 * @param string $accountId
	 *
	 * @return static
	 */
	public static function withAccountId(string $accountId): self
	{
		$message = 'Account Id: ' . $accountId . ' does not exist';
		return new self($message);
	}
}
