<?php

declare(strict_types=1);

namespace SocialNetwork\Models\Account\Exceptions;

use Exception;

final class AccountAlreadyFollowingGroupException extends Exception
{
	/**
	 * @param string $groupId
	 *
	 * @return static
	 */
	public static function withGroupId(string $groupId): self
	{
		$message = 'The logged account already follows the group id: ' . $groupId;
		return new self($message);
	}
}
