<?php

declare(strict_types=1);

namespace SocialNetwork\Models\Account\Exceptions;


use Exception;

final class AccountDoesNotFollowGroupException extends Exception
{
	/**
	 * @param string $groupId
	 *
	 * @return static
	 */
	public static function withGroupId(string $groupId): self
	{
		$message = 'The logged account does not follows group: ' . $groupId;
		return new self($message);
	}
}
