<?php

declare(strict_types=1);

namespace SocialNetwork\Models\Account\Exceptions;


use Exception;

final class AccountDoesNotFollowAccountException extends Exception
{
	/**
	 * @param string $accountId
	 *
	 * @return static
	 */
	public static function withFollowingId(string $accountId): self
	{
		$message = 'The logged account does not follow: ' . $accountId;
		return new self($message);
	}
}
