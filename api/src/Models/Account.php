<?php

declare(strict_types=1);

namespace SocialNetwork\Models;


use DateTimeImmutable;
use Exception;
use Ramsey\Uuid\Uuid;

final class Account
{
	/**
	 * @var string
	 */
	private $accountId;

	/**
	 * @var string
	 */
	private $firstName;

	/**
	 * @var string
	 */
	private $lastName;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $profilePicture;

	/**
	 * @var string
	 */
	private $bio;

	/**
	 * @var string
	 */
	private $password;

	/**
	 * @var string
	 */
	private $login;

	/**
	 * @var string
	 */
	private $gender;

	/**
	 * @var string
	 */
	private $phone;

	/**
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 */
	private $country;

	/**
	 * @var string
	 */
	private $countryCode;

	/**
	 * @var DateTimeImmutable
	 */
	private $birthDate;

	/**
	 * @var DateTimeImmutable
	 */
	private $dateCreated;

	/**
	 * @var DateTimeImmutable
	 */
	private $dateUpdated;

	/**
	 * @var DateTimeImmutable|null
	 */
	private $dateRevoked;

	/**
	 * @var Account[]
	 */
	private $following;

	/**
	 * @var Account[]
	 */
	private $followers;

	/**
	 * @var bool
	 */
	private $followedByUser;

	/**
	 * @var bool
	 */
	private $followingUser;

	/**
	 * @var bool
	 */
	private $friendship;

	/**
	 * @var Group[]
	 */
	private $groups;

	/**
	 * Account constructor.
	 *
	 * @param array|null $payload
	 *
	 * @throws Exception
	 */
	public function __construct(array $payload = null)
	{
		if ($payload !== null) {
			$this->fromPayload($payload);
		}
	}

	/**
	 * @param array $payload
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function fromPayload(array $payload): self
	{
		$this->accountId = $payload['accountId'] ?? Uuid::uuid4()->toString();
		$this->firstName = $payload['firstName'];
		$this->lastName = $payload['lastName'];
		$this->email = $payload['email'];
		$this->password = $payload['password'] ?? '';
		$this->profilePicture = $payload['profilePicture'];
		$this->bio = $payload['bio'];
		$this->birthDate = $payload['birthDate'];
		$this->login = $payload['login'];
		$this->gender = $payload['gender'];
		$this->phone = $payload['phone'];
		$this->city = $payload['city'];
		$this->country = $payload['country'];
		$this->countryCode = $payload['countryCode'];
		$this->dateCreated = $payload['dateCreated'];
		$this->dateUpdated = $payload['dateUpdated'];
		$this->dateRevoked = $payload['dateRevoked'] ?? null;

		$this->followedByUser = $payload['followedByUser'] ?? false;
		$this->followingUser = $payload['followingUser'] ?? false;
		$this->friendship = ($this->followedByUser && $this->followingUser);


		return $this;
	}

	/**
	 * @param array $followers
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function withFollowers(array $followers = null): self
	{

		$clone = clone $this;

		if ($followers !== null) {
			if ($clone->followers === null) {
				$clone->followers = [];
			}

			foreach ($followers as $follower) {
				$clone->followers[] = Account::createFromPayload($follower);
			}
		}

		return $clone;
	}

	/**
	 * @param array $following
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function withFollowing(array $following = null): self
	{
		$clone = clone $this;

		if ($following !== null) {

			if ($clone->following === null) {
				$clone->following = [];
			}

			foreach ($following as $follower) {
				$clone->following[] = Account::createFromPayload($follower);
			}
		}

		return $clone;
	}

	/**
	 * @param array|null $groups
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function withGroups(array $groups = null): self
	{

		$clone = clone $this;

		if ($groups !== null) {
			if ($clone->groups === null) {
				$clone->groups = [];
			}

			foreach ($groups as $group) {
				$clone->groups[] = Group::createFromPayload($group);
			}
		}

		return $clone;
	}

	/**
	 * @return array
	 */
	public function toArray(): array
	{

		$data = [
			'accountId' => $this->accountId,
			'firstName' => $this->firstName,
			'lastName' => $this->lastName,
			'email' => $this->email,
			'profilePicture' => $this->profilePicture,
			'bio' => $this->bio,
			'login' => $this->login,
			'gender' => $this->gender,
			'phone' => $this->phone,
			'city' => $this->city,
			'country' => $this->country,
			'countryCode' => $this->countryCode,
			'birthDate' => $this->birthDate,
			'dateCreated' => $this->dateCreated,
			'dateUpdated' => $this->dateUpdated,
			'dateRevoked' => $this->dateRevoked,
			'following' => null,
			'followers' => null,
			'followedByUser' => $this->followedByUser,
			'followingUser' => $this->followingUser,
			'friendship' => $this->friendship
		];

		if (!empty($this->followers)) {
			/** @var Account $follower */
			foreach ($this->followers as $follower) {
				$data['followers'][] = $follower->toDisplayAccount();
			}
		}

		if (!empty($this->following)) {
			/** @var Account $follower */
			foreach ($this->following as $follower) {
				$data['following'][] = $follower->toDisplayAccount();
			}
		}

		if (!empty($this->groups)) {
			/** @var Group $group */
			foreach ($this->groups as $group) {
				$data['groups'][] = $group->toArray();
			}
		}

		return $data;

	}

	/**
	 * @return array
	 */
	public function toDisplayAccount(): array
	{
		return [
			'accountId' => $this->accountId,
			'name' => sprintf('%s %s', $this->firstName, $this->lastName),
			'profilePicture' => $this->profilePicture,
			'login' => $this->login,
			'gender' => $this->gender,
			'city' => $this->city,
			'country' => $this->country,
			'countryCode' => $this->countryCode,
			'followingDate' => $this->dateCreated
		];
	}

	/**
	 * @return array
	 */
	public function toDisplayShortAccount(): array
	{
		return [
			'accountId' => $this->accountId,
			'name' => sprintf('%s %s', $this->firstName, $this->lastName),
			'profilePicture' => $this->profilePicture,
			'gender' => $this->gender,
			'city' => $this->city,
			'country' => $this->country,
			'followingDate' => $this->dateCreated
		];
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function accountId(): string
	{
		if ($this->accountId === null) {
			$this->accountId = Uuid::uuid4()->toString();
		}

		return $this->accountId;
	}

	/**
	 * @return string
	 */
	public function firstName(): string
	{
		return $this->firstName;
	}

	/**
	 * @return string
	 */
	public function lastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @return string
	 */
	public function email(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function password(): string
	{
		return $this->password;
	}

	/**
	 * @return string
	 */
	public function profilePicture(): string
	{
		return $this->profilePicture;
	}

	/**
	 * @return string
	 */
	public function bio(): string
	{
		return $this->bio;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function birthDate(): DateTimeImmutable
	{
		return $this->birthDate;
	}

	/**
	 * @return string
	 */
	public function login(): string
	{
		return $this->login;
	}

	/**
	 * @return string
	 */
	public function gender(): string
	{
		return $this->gender;
	}

	/**
	 * @return string
	 */
	public function phone(): string
	{
		return $this->phone;
	}

	/**
	 * @return string
	 */
	public function city(): string
	{
		return $this->city;
	}

	/**
	 * @return string
	 */
	public function country(): string
	{
		return $this->country;
	}

	/**
	 * @return string
	 */
	public function countryCode(): string
	{
		return $this->countryCode;
	}


	/**
	 * @return DateTimeImmutable
	 */
	public function dateCreated(): DateTimeImmutable
	{
		return $this->dateCreated;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function dateUpdated(): DateTimeImmutable
	{
		return $this->dateUpdated;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function dateRevoked(): ?DateTimeImmutable
	{
		return $this->dateRevoked;
	}

	/**
	 * @return array
	 */
	public function followers(): array
	{
		return $this->followers ?? [];
	}

	/**
	 * @return array
	 */
	public function following(): array
	{
		return $this->following ?? [];
	}

	/**
	 * @return bool
	 */
	public function followedByUser(): bool
	{
		return $this->followedByUser;
	}

	/**
	 * @return bool
	 */
	public function followingUser(): bool
	{
		return $this->followingUser;
	}

	/**
	 * @return bool
	 */
	public function friendship(): bool
	{
		return $this->friendship;
	}

	/**
	 * @return array
	 */
	public function groups(): array
	{
		return $this->groups;
	}

	/**
	 * STATIC FUNCTIONS
	 */

	/**
	 * @param array $payload
	 *
	 * @return static
	 * @throws Exception
	 */
	public static function createFromPayload(array $payload): self
	{
		return new self($payload);
	}
}
