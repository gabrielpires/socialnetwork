<?php

declare(strict_types=1);

namespace SocialNetwork\Projections;


final class Projector extends AbstractProjection
{
	/****************** ACCOUNT FUNCTIONS *******************/

	/**
	 * @param array $data
	 *
	 * @return bool
	 */
	public function insertAccountToken(array $data): bool
	{
		$this->revokeAccountTokens($data['accountId']);

		$query = 'INSERT INTO `account_token` (account_token_id, account_id, token) VALUES (:accountTokenId, :accountId, :token);';

		return $this->execute($query,
			[
				'accountTokenId' => $data['accountTokenId'],
				'accountId' => $data['accountId'],
				'token' => $data['token']
			]
		);
	}

	/**
	 * @param string $accountId
	 *
	 * @return bool
	 */
	public function revokeAccountTokens(string $accountId): bool
	{
		$query = 'UPDATE `account_token` SET date_revoked = current_timestamp WHERE account_id = :accountId;';

		return $this->execute($query,
			[
				'accountId' => $accountId
			]
		);
	}

	/**
	 * @param string $accountId
	 * @param string $followingId
	 *
	 * @return bool
	 */
	public function followAccount(string $accountId, string $followingId): bool
	{
		$query = 'INSERT INTO account_following (account_id, following_id) VALUES (:accountId, :followingId);';

		return $this->execute(
			$query,
			[
				'accountId' => $accountId,
				'followingId' => $followingId
			]
		);
	}

	/**
	 * @param string $accountId
	 * @param string $followingId
	 *
	 * @return bool
	 */
	public function unfollowAccount(string $accountId, string $followingId): bool
	{
		$query = 'DELETE FROM account_following WHERE account_id = :accountId AND following_id = :followingId;';

		return $this->execute(
			$query,
			[
				'accountId' => $accountId,
				'followingId' => $followingId
			]
		);
	}

	/**
	 * @param string $accountId
	 * @param string $groupId
	 *
	 * @return bool
	 */
	public function followGroup(string $accountId, string $groupId): bool
	{
		$query = 'INSERT INTO account_groups (account_id, group_id) VALUES (:accountId, :groupId);';

		return $this->execute(
			$query,
			[
				'accountId' => $accountId,
				'groupId' => $groupId
			]
		);
	}

	/**
	 * @param string $accountId
	 * @param string $groupId
	 *
	 * @return bool
	 */
	public function unfollowGroup(string $accountId, string $groupId): bool
	{
		$query = 'DELETE FROM account_groups WHERE account_id = :accountId AND group_id = :groupId;';

		return $this->execute(
			$query,
			[
				'accountId' => $accountId,
				'groupId' => $groupId
			]
		);
	}

	/****************** GROUP FUNCTIONS *******************/
}
