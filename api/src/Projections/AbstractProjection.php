<?php

declare(strict_types=1);

namespace SocialNetwork\Projections;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use \PDO;
use Psr\Log\LoggerInterface;


abstract class AbstractProjection
{
	/**
	 * @var Connection
	 */
	private $connection;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function __construct(Connection $connection, LoggerInterface $logger)
	{
		$this->connection = $connection;
		$this->logger = $logger;
	}


	/**
	 * @param string $query
	 * @param array  $parameters
	 *
	 * @return bool
	 */
	protected function execute(string $query, array $parameters = []): bool
	{

		try {
			$statement = $this->connection->prepare($query);

			return $statement->execute($parameters);

		} catch (DBALException $exception) {
			$message = sprintf('File: %s:%s | Message: %s | Stacktrace: %s',
				$exception->getFile(),
				$exception->getLine(),
				$exception->getMessage(),
				$exception->getTraceAsString()
			);
			$this->logger->info('Unable to fetch data from database');
			$this->logger->error($message);

			return false;
		}
	}


	/**
	 * @param string $query
	 * @param array  $parameters
	 *
	 * @return array|null
	 */
	protected function fetchAll(string $query, array $parameters = []): ?array
	{

		try {
			$statement = $this->connection->prepare($query);
			$statement->execute($parameters);
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);

			return $this->capitalizeResults($result);

		} catch (DBALException $exception) {
			$message = sprintf('File: %s:%s | Message: %s | Stacktrace: %s',
				$exception->getFile(),
				$exception->getLine(),
				$exception->getMessage(),
				$exception->getTraceAsString()
			);
			$this->logger->info('Unable to fetch data from database');
			$this->logger->error($message);

			return null;
		}
	}

	/**
	 * @param string $query
	 * @param array  $parameters
	 *
	 * @return array|null
	 */
	protected function fetchOne(string $query, array $parameters = []): ?array
	{

		try {
			$statement = $this->connection->prepare($query);
			$statement->execute($parameters);
			$result = $statement->fetch(PDO::FETCH_ASSOC);

			if ($result === false) {
				return null;
			}

			return $this->capitalizeResult($result);

		} catch (DBALException $exception) {
			$message = sprintf('File: %s:%s | Message: %s | Stacktrace: %s',
				$exception->getFile(),
				$exception->getLine(),
				$exception->getMessage(),
				$exception->getTraceAsString()
			);
			$this->logger->info('Unable to fetch data from database');
			$this->logger->error($message);

			return null;
		}
	}

	/**
	 * @param array $rows
	 *
	 * @return array
	 */
	protected function capitalizeResults(array $rows): array
	{
		$capitalizedResults = [];
		foreach ($rows as $row) {
			$capitalizedResults[] = $this->capitalizeResult($row);
		}

		return $capitalizedResults;
	}

	/**
	 * @param array $result
	 *
	 * @return array
	 */
	protected function capitalizeResult(array $result): array
	{
		$capitalizedResult = [];

		array_walk($result, function ($value, $key) use (&$capitalizedResult) {

			$key = preg_replace_callback(
				'/_([a-z])/',
				function ($matches) {
					return mb_strtoupper($matches[1], 'UTF-8');
				},
				$key
			);

			$capitalizedResult[$key] = $value;
		});

		return $capitalizedResult;
	}
}
