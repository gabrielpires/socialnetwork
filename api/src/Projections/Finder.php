<?php /** @noinspection ALL */

declare(strict_types=1);

namespace SocialNetwork\Projections;


final class Finder extends AbstractProjection
{
	/****************** ACCOUNT FUNCTIONS *******************/

	/**
	 * @return string
	 */
	private function baseAccountQuery(): string
	{
		return 'SELECT 
					account_id, 
					first_name, 
					last_name, email, 
					password, 
					bio, 
					profile_picture, 
					birth_date, 
					login, 
					phone, 
					gender, 
					city, 
					country, 
					country_code, 
					date_created, 
					date_updated, 
					date_revoked 
				FROM account';
	}

	/**
	 * @param string $accountId
	 *
	 * @return array|null
	 */
	public function getAccountById(string $accountId): ?array
	{
		$query = $this->baseAccountQuery() . ' WHERE account_id = :accountId LIMIT 1;';

		return $this->fetchOne(
			$query,
			[
				'accountId' => $accountId
			]
		);
	}

	/**
	 * @return array|null
	 */
	public function getAllAccounts(): ?array
	{
		return $this->fetchAll($this->baseAccountQuery());
	}

	/**
	 * @param string $login
	 *
	 * @return array|null
	 */
	public function getAccountByLogin(string $login): ?array
	{
		$query = 'SELECT account_id, login, password FROM account WHERE login = :login LIMIT 1;';

		return $this->fetchOne(
			$query,
			[
				'login' => $login
			]
		);
	}

	/**
	 * @param string $accountId
	 * @param string $token
	 *
	 * @return array|null
	 */
	public function checkTokenForAccount(string $accountId, string $token): ?array
	{
		$query = 'SELECT account_token_id FROM account_token WHERE account_id = :accountId and token = :token and date_revoked IS NULL LIMIT 1;';

		return $this->fetchOne(
			$query,
			[
				'accountId' => $accountId,
				'token' => $token
			]
		);
	}

	/**
	 * @param string $accountId
	 * @param string $followingId
	 *
	 * @return bool
	 */
	public function isFollowingAccount(string $accountId, string $followingId): bool
	{

		$query = 'SELECT account_id FROM account_following WHERE account_id = :accountId AND following_id = :followingId;';

		$data = $this->fetchOne(
			$query,
			[
				'accountId' => $accountId,
				'followingId' => $followingId
			]
		);

		return ($data === null) ? false : true;

	}

	/**
	 * @param string $accountId
	 * @param string $followingId
	 *
	 * @return bool
	 */
	public function isFollowingGroup(string $accountId, string $groupId): bool
	{

		$query = 'SELECT account_id FROM account_groups WHERE account_id = :accountId AND group_id = :groupId;';

		$data = $this->fetchOne(
			$query,
			[
				'accountId' => $accountId,
				'groupId' => $groupId
			]
		);

		return ($data === null) ? false : true;

	}

	/**
	 * @param string $accountId
	 *
	 * @return array|null
	 */
	public function getAccountFollowing(string $accountId): ?array
	{
		$query = '
			SELECT 
					a.account_id, 
					a.first_name, 
					a.last_name, email, 
					a.bio, 
					a.profile_picture, 
					a.birth_date, 
					a.login, 
					a.phone, 
					a.gender, 
					a.city, 
					a.country, 
					a.country_code, 
					f.date_created, 
					a.date_updated, 
					a.date_revoked 
				FROM account AS a
				INNER JOIN account_following AS f ON a.account_id = f.following_id
				WHERE 
					f.account_id = :accountId 
		';

		return $this->fetchAll(
			$query,
			[
				'accountId' => $accountId
			]
		);
	}

	/**
	 * @param string $accountId
	 *
	 * @return array|null
	 */
	public function getAccountFollowers(string $accountId): ?array
	{
		$query = '
			SELECT  
					a.account_id, 
					a.first_name, 
					a.last_name, email, 
					a.bio, 
					a.profile_picture, 
					a.birth_date, 
					a.login, 
					a.phone, 
					a.gender, 
					a.city, 
					a.country, 
					a.country_code, 
					f.date_created, 
					a.date_updated, 
					a.date_revoked 
				FROM account AS a
				INNER JOIN account_following AS f ON a.account_id = f.account_id
				WHERE f.following_id = :accountId 
		';

		return $this->fetchAll(
			$query,
			[
				'accountId' => $accountId
			]
		);
	}

	/**
	 * @param string $accountId
	 *
	 * @return array|null
	 */
	public function getAccountGroups(string $accountId): ?array
	{
		$query = '
			SELECT  
					g.`group_id`, 
					g.group_name, 
					g.category,
					g.profile_picture, 
					g.header_picture, 
					g.description, 
					g.date_created, 
					g.date_updated, 
					g.date_revoked
				FROM `group` AS g
				INNER JOIN account_groups AS ag ON g.group_id = ag.group_id
				WHERE ag.account_id = :accountId 
		';

		return $this->fetchAll(
			$query,
			[
				'accountId' => $accountId
			]
		);
	}

	/**
	 * @param string|null $country
	 * @param string|null $groupId
	 *
	 * @return array|null
	 */
	public function searchAccounts(string $country = null, string $groupId = null)
	{
		$parameters = [];
		$whereFields = [];
		$query = '	
			SELECT
					DISTINCT
					a.account_id, 
					a.first_name, 
					a.last_name,   
					a.profile_picture,  
					a.country
			FROM account as a 
			INNER JOIN account_groups AS ag ON a.account_id = ag.account_id';

		if (!empty($country)) {
			$whereFields[] = ' a.country = :country ';
			$parameters['country'] = $country;
		}

		if (!empty($groupId)) {
			$whereFields[] = ' ag.group_id = :groupId ';
			$parameters['groupId'] = $groupId;
		}

		if (!empty($parameters)) {
			$query .= ' WHERE ' . implode(' AND ', $whereFields);
		}

		$query .= ' ORDER BY a.first_name, a.last_name';

		return $this->fetchAll(
			$query,
			$parameters
		);
	}

	/**
	 * @return array|null
	 */
	public function getAllCountriesForFilter(): ?array
	{
		return $this->fetchAll('SELECT DISTINCT country FROM account ORDER BY country;');
	}

	/****************** GROUP FUNCTIONS *******************/

	/**
	 * @return string
	 */
	private function baseGroupQuery(): string
	{
		return 'SELECT `group_id`, group_name, category, profile_picture, header_picture, description, date_created, date_updated, date_revoked FROM `group`';
	}

	/**
	 * @return array|null
	 */
	public function getAllGroups(): ?array
	{
		return $this->fetchAll($this->baseGroupQuery());
	}

	/**
	 * @return array|null
	 */
	public function getAllGroupsForFilter(): ?array
	{
		return $this->fetchAll('SELECT group_id, group_name FROM `group` ORDER BY group_name;');
	}

	/**
	 * @param string $groupId
	 *
	 * @return array|null
	 */
	public function getGroupById(string $groupId): ?array
	{
		$query = $this->baseGroupQuery() . 'WHERE group_id = :groupId';

		return $this->fetchOne(
			$query,
			[
				'groupId' => $groupId
			]
		);
	}

	/**
	 * @param string $groupId
	 *
	 * @return array
	 */
	public function getGroupAccounts(string $groupId): ?array
	{
		$query = 'SELECT  
					a.account_id, 
					a.first_name, 
					a.last_name, email, 
					a.bio, 
					a.profile_picture, 
					a.birth_date, 
					a.login, 
					a.phone, 
					a.gender, 
					a.city, 
					a.country, 
					a.country_code, 
					ag.date_created, 
					a.date_updated, 
					a.date_revoked 
				FROM account AS a
				INNER JOIN account_groups AS ag ON a.account_id = ag.account_id
				WHERE ag.group_id = :groupId ';

		return $this->fetchAll(
			$query,
			[
				'groupId' => $groupId
			]
		);

	}

}
