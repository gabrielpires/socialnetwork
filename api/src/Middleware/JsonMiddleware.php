<?php

declare(strict_types=1);


namespace SocialNetwork\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


final class JsonMiddleware implements MiddlewareInterface
{
	/**
	 * This middleware is designed to take and parse the incoming JSON to put it in the parsedBody.
	 *
	 * @param ServerRequestInterface  $request
	 * @param RequestHandlerInterface $handler
	 *
	 * @return ResponseInterface
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$body = (string)$request->getBody();

		$restJson = json_decode($body, true) ?? [];

		$request = $request->withParsedBody($restJson ?? []);

		return $handler->handle($request);
	}
}
