<?php

declare(strict_types=1);


namespace SocialNetwork\Middleware;

use Exception;
use League\Route\Http\Exception\BadRequestException;
use Opis\JsonSchema\Schema;
use Opis\JsonSchema\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


final class ValidationMiddleware implements MiddlewareInterface
{
	/**
	 * This middleware is designed to validate the incoming payloads
	 *
	 * @param ServerRequestInterface  $request
	 * @param RequestHandlerInterface $handler
	 *
	 * @return ResponseInterface
	 * @throws BadRequestException
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$jsonSchemaFilePath = $this->buildSchemaFilePath(
			$request->getUri(),
			$request->getMethod()
		);

		//if the file does not exist we will scape validation
		if (empty($jsonSchemaFilePath) || !file_exists($jsonSchemaFilePath)) {
			return $handler->handle($request);
		}
		//opis/json-schema requires an object to validate
		$body = json_decode(json_encode($request->getParsedBody()));

		$schema = Schema::fromJsonString(
			$this->getSchemaContent($jsonSchemaFilePath)
		);

		$validation = new Validator();

		$result = $validation->schemaValidation($body, $schema);

		if ($result->isValid()) {
			return $handler->handle($request);
		}

		$message = [
			'reason' => 'Invalid payload.',
			'details' => []
		];

		$errors = $result->getErrors();

		foreach ($errors as $error) {
			$message['details'][] = $error->keyword() . ':' . implode(' ', $error->keywordArgs());
		}

		throw new BadRequestException(json_encode($message));

	}

	/**
	 * @param UriInterface $uri
	 * @param string       $httpMethod
	 *
	 * @return string|null
	 */
	private function buildSchemaFilePath(UriInterface $uri, string $httpMethod): ?string
	{
		try {
			$path = $uri->getPath();
			//removing the first slash from the path
			$path = substr($path, 1);
			$path = explode('/', $path);

			//the configuration of the files will be: domain/action/METHOD.json for this reason, I'll pick the index 0 and 1 of the path
			return sprintf('%s/../Validations/%s/%s/%s.json',
				__DIR__,
				strtolower($path[0]),
				strtolower($path[1]),
				$httpMethod
			);

		} catch (Exception $exception) {
			return null;
		}

	}

	/**
	 * @param string $jsonSchemaFilePath
	 *
	 * @return string
	 */
	private function getSchemaContent(string $jsonSchemaFilePath): string
	{
		$handler = fopen($jsonSchemaFilePath, 'r');
		$content = fread($handler, filesize($jsonSchemaFilePath));
		fclose($handler);
		unset($handler);

		return $content;

	}
}
