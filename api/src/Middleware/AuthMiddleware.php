<?php

declare(strict_types=1);

namespace SocialNetwork\Middleware;

use Exception;
use League\Route\Http\Exception\UnauthorizedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use SocialNetwork\Controllers\Exceptions\SomethingWentWrongException;
use SocialNetwork\Modules\JWTHandler;
use SocialNetwork\Projections\Finder;

final class AuthMiddleware implements MiddlewareInterface
{
	/**
	 * @var Finder
	 */
	private $finder;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * AuthMiddleware constructor.
	 *
	 * @param Finder          $finder
	 * @param LoggerInterface $logger
	 */
	public function __construct(Finder $finder, LoggerInterface $logger)
	{
		$this->finder = $finder;
		$this->logger = $logger;
	}

	/**
	 * This middleware is designed to validate the access token from the user
	 *
	 * @param ServerRequestInterface  $request
	 * @param RequestHandlerInterface $handler
	 *
	 * @return ResponseInterface
	 * @throws UnauthorizedException
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$authHeader = $request->getHeader('Authorization');

		if (empty($authHeader)) {
			throw new UnauthorizedException();
		}

		try {

			$token = current(sscanf(current($authHeader), 'Bearer %s'));

			if (empty($token)) {
				throw SomethingWentWrongException::withMessage('Unable to validate token. Reason: empty value. Check header\`s request.');
			}

			$tokenObject = JWTHandler::decodeToken($token);

			if ($tokenObject === null) {
				throw new UnauthorizedException();
			}


			if (!$this->isValidToken($token, $tokenObject)) {
				throw new UnauthorizedException();
			}

		} catch (Exception $exception) {

			throw new UnauthorizedException();
		}

		$request = $request
			->withAttribute('token', $token)
			->withAttribute('accountId', $tokenObject['data']['accountId']);

		return $handler->handle($request);
	}

	/**
	 * @param string $token
	 * @param array  $tokenObject
	 *
	 * @return bool
	 */
	private function isValidToken(string $token, array $tokenObject): bool
	{
		$accountId = $tokenObject['data']['accountId'] ?? '';
		$accountTokenId = $tokenObject['data']['accountTokenId'];

		if (empty($accountId) || empty($accountTokenId)) {
			return false;
		}

		$accountToken = $this->finder->checkTokenForAccount($accountId, $token);

		if (empty($accountToken) || $accountToken['accountTokenId'] !== $accountTokenId) {
			return false;
		}

		return true;
	}
}
