<?php

declare(strict_types=1);

use GuzzleHttp\Psr7\ServerRequest;
use League\Route\RouteGroup;
use League\Route\Router;
use League\Route\Strategy\JsonStrategy;
use SocialNetwork\Controllers\AccountController;
use SocialNetwork\Controllers\GroupController;
use SocialNetwork\Middleware\JsonMiddleware;
use SocialNetwork\Middleware\ValidationMiddleware as ValidationMiddlewareAlias;


$request = ServerRequest::fromGlobals();

$responseFactory = new Zend\Diactoros\ResponseFactory;
$jsonStrategy = new JsonStrategy($responseFactory);
$router = new Router();
$router
	->middleware(new JsonMiddleware())
	->middleware(new ValidationMiddlewareAlias());

$authMiddleware = $container->get('app.middleware.auth');

$router->setStrategy($jsonStrategy);

$router->map('GET', '/', static function () {
	return [];
});

/**
 * Account Route Group
 */

$router->group('/account', function (RouteGroup $route) use ($container, $authMiddleware) {
	/** @var AccountController $accountController */
	$accountController = $container->get('app.controllers.account');

	$route->post('/auth', static function (ServerRequest $request, array $args) use ($accountController) {
		return $accountController->login($request->getParsedBody());
	});

	$route->get('/profile', static function (ServerRequest $request, array $args) use ($accountController) {

		return $accountController->getProfile(
			(string)$request->getAttribute('accountId')
		);

	})->middleware($authMiddleware);

	$route->get('/search', static function (ServerRequest $request, array $args) use ($accountController) {

		return $accountController->searchAccounts(
			$request->getQueryParams()['country'],
			$request->getQueryParams()['groupId']
		);

	})->middleware($authMiddleware);

	$route->get('/profile/{accountId}', static function (ServerRequest $request, array $args) use ($accountController) {

		return $accountController->getProfile(
			$args['accountId'],
			$request->getAttribute('accountId')
		);

	})->middleware($authMiddleware);

	//add a new friendship based on logged user
	$route->post('/follow', static function (ServerRequest $request, array $args) use ($accountController) {

		return $accountController->followAccount(
			(string)$request->getAttribute('accountId'),
			$request->getParsedBody()['followingId']
		);

	})->middleware($authMiddleware);

	//delete a new friendship based on logged user
	$route->delete('/follow', static function (ServerRequest $request, array $args) use ($accountController) {
		return $accountController->unfollowAccount(
			(string)$request->getAttribute('accountId'),
			$request->getParsedBody()['followingId']
		);

	})->middleware($authMiddleware);

});

$router->group('/group', function (RouteGroup $route) use ($container, $authMiddleware) {
	/** @var GroupController $groupController */
	$groupController = $container->get('app.controllers.group');

	$route->get('/all', static function (ServerRequest $request, array $args) use ($groupController) {
		return $groupController->getAll();
	});

	$route->get('/page/{groupId}', static function (ServerRequest $request, array $args) use ($groupController) {

		return $groupController->getPage(
			$args['groupId'],
			$request->getAttribute('accountId')
		);

	})->middleware($authMiddleware);

	$route->post('/follow', static function (ServerRequest $request, array $args) use ($groupController) {
		return $groupController->followGroup(
			(string)$request->getAttribute('accountId'),
			$request->getParsedBody()['groupId']
		);
	})->middleware($authMiddleware);

	$route->delete('/follow', static function (ServerRequest $request, array $args) use ($groupController) {
		return $groupController->unfollowGroup(
			(string)$request->getAttribute('accountId'),
			$request->getParsedBody()['groupId']
		);
	})->middleware($authMiddleware);

});


$response = $router->dispatch($request);
$response->withStatus(401);
(new Zend\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);
