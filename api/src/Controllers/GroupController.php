<?php

declare(strict_types=1);

namespace SocialNetwork\Controllers;

use Exception;
use SocialNetwork\Models\Account\Exceptions\AccountAlreadyFollowingGroupException;
use SocialNetwork\Models\Account\Exceptions\AccountDoesNotFollowGroupException;
use SocialNetwork\Models\Group\Exceptions\GroupDoesNotExistException;
use SocialNetwork\Models\Group;

final class GroupController extends AbstractController
{
	/**
	 * @return array
	 */
	public function getAll(): array
	{
		return $this->getResponseBody('All Groups', $this->finder->getAllGroups() ?? []);
	}

	/**
	 * @param string      $groupId
	 *
	 * @param string|null $accountId
	 *
	 * @return array
	 * @throws GroupDoesNotExistException
	 * @throws Exception
	 */
	public function getPage(string $groupId, string $accountId = null): array
	{

		$group = $this->finder->getGroupById($groupId);

		if ($group === null) {
			throw GroupDoesNotExistException::withGroupId($groupId);
		}

		if (!empty($accountId)) {
			$followedByLoggedUser = $this->finder->isFollowingGroup($accountId, $groupId);
			$group['followedByUser'] = $followedByLoggedUser;
		}

		$group = Group::createFromPayload($group);

		$group = $group->withFollowers(
			$this->finder->getGroupAccounts($groupId)
		);


		return $this->getResponseBody('Page of Group', $group->toArray());

	}

	/**
	 * @param string $accountId
	 * @param string $groupId
	 *
	 * @return array
	 * @throws AccountAlreadyFollowingGroupException
	 * @throws GroupDoesNotExistException
	 */
	public function followGroup(string $accountId, string $groupId): array
	{
		$group = $this->finder->getGroupById($groupId);

		if ($group === null) {
			throw GroupDoesNotExistException::withGroupId($groupId);
		}

		$result = $this->projector->followGroup($accountId, $groupId);

		if ($result === false) {
			throw AccountAlreadyFollowingGroupException::withGroupId($groupId);
		}

		return $this->getResponseBody('Following new group', ['groupId' => $groupId]);

	}

	/**
	 * @param string $accountId
	 * @param string $groupId
	 *
	 * @return array
	 * @throws AccountDoesNotFollowGroupException
	 * @throws GroupDoesNotExistException
	 */
	public function unfollowGroup(string $accountId, string $groupId): array
	{
		$group = $this->finder->getGroupById($groupId);

		if ($group === null) {
			throw GroupDoesNotExistException::withGroupId($groupId);
		}

		$isFollowing = $this->finder->isFollowingGroup($accountId, $groupId);

		if (!$isFollowing) {
			throw AccountDoesNotFollowGroupException::withGroupId($groupId);
		}

		$this->projector->unfollowGroup($accountId, $groupId);

		return $this->getResponseBody('Unfollowing group', ['groupId' => $groupId]);

	}

}
