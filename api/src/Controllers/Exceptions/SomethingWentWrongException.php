<?php declare(strict_types=1);

namespace SocialNetwork\Controllers\Exceptions;

use Exception;
use League\Route\Http\Exception as HttpException;
use Throwable;

class SomethingWentWrongException extends HttpException
{
	/**
	 * Constructor
	 *
	 * @param string    $message
	 * @param Exception $previous
	 * @param int       $code
	 */
	public function __construct(string $message = 'Something went wrong', ?Exception $previous = null, int $code = 0)
	{
		parent::__construct(500, $message, $previous, [], $code);
	}

	/**
	 * @param Throwable $exception
	 *
	 * @return static
	 */
	public static function withException(Throwable $exception): self
	{
		$message = 'No details';

		if (ENVIRONMENT === 'DEVELOPMENT') {
			$message = sprintf('File: %s:%s | Message: %s | Stacktrace: %s',
				$exception->getFile(),
				$exception->getLine(),
				$exception->getMessage(),
				$exception->getTraceAsString()
			);
		}

		return new self('Something went wrong: ' . $message);
	}

	/**
	 * @param string $message
	 *
	 * @return static
	 */
	public static function withMessage(string $message): self
	{
		return new self('Something went wrong: ' . $message);
	}
}
