<?php

declare(strict_types=1);

namespace SocialNetwork\Controllers;

use Psr\Log\LoggerInterface;
use SocialNetwork\Projections\Finder;
use SocialNetwork\Projections\Projector;

abstract class AbstractController
{
	/**
	 * @var Projector
	 */
	protected $projector;

	/**
	 * @var Finder
	 */
	protected $finder;

	/**
	 * @var LoggerInterface
	 */
	protected $logger;

	/**
	 * AbstractController constructor.
	 *
	 * @param Projector       $projector
	 * @param Finder          $finder
	 * @param LoggerInterface $logger
	 */
	public function __construct(Projector $projector, Finder $finder, LoggerInterface $logger)
	{
		$this->projector = $projector;
		$this->finder = $finder;
		$this->logger = $logger;
	}

	/**
	 * @param string $message
	 * @param array  $data
	 *
	 * @return array
	 */
	protected function getResponseBody(string $message, array $data = null)
	{
		return [
			'message' => $message,
			'data' => $data
		];
	}
}
