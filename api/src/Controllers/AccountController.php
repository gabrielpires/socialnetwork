<?php

declare(strict_types=1);

namespace SocialNetwork\Controllers;


use Exception;
use League\Route\Http\Exception\UnauthorizedException;
use Ramsey\Uuid\Uuid;
use SocialNetwork\Controllers\Exceptions\SomethingWentWrongException;
use SocialNetwork\Models\Account;
use SocialNetwork\Models\Account\Exceptions\AccountAlreadyFollowingAccountException;
use SocialNetwork\Models\Account\Exceptions\AccountDoesNotExistException;
use SocialNetwork\Models\Account\Exceptions\AccountDoesNotFollowAccountException;
use SocialNetwork\Modules\JWTHandler;

final class AccountController extends AbstractController
{
	/**
	 * @param array $payload
	 *
	 * @return array
	 * @throws UnauthorizedException
	 * @throws Exception
	 */
	public function login(array $payload): array
	{
		$account = $this->finder->getAccountByLogin($payload['login']);

		if ($account === null) {
			throw new UnauthorizedException();
		}

		if (!password_verify($payload['password'], $account['password'])) {
			throw new UnauthorizedException();
		}

		$accountProfile = Account::createFromPayload(
			$this->finder->getAccountById($account['accountId'])
		);

		$accountTokenId = Uuid::uuid4()->toString();
		$accountProfile = $accountProfile->toArray();
		$accountProfile['accountTokenId'] = $accountTokenId;

		$token = JWTHandler::issueToken($accountProfile);

		$this->projector->insertAccountToken([
			'accountTokenId' => $accountTokenId,
			'accountId' => $account['accountId'],
			'token' => $token
		]);

		return $this->getResponseBody(
			'Access Authorized',
			[
				'accessToken' => $token
			]
		);
	}

	/**
	 * @param string $accountId
	 *
	 * @param string $requestingAccountId
	 *
	 * @return array
	 * @throws AccountDoesNotExistException
	 * @throws SomethingWentWrongException
	 */
	public function getProfile(string $accountId, string $requestingAccountId = null): array
	{
		$account = $this->finder->getAccountById($accountId);

		if ($account === null) {
			throw AccountDoesNotExistException::withAccountId($accountId);
		}

		try {
			if (!empty($requestingAccountId)) {
				$account['followedByUser'] = $this->finder->isFollowingAccount($accountId, $requestingAccountId);
				$account['followingUser'] = $this->finder->isFollowingAccount($requestingAccountId, $accountId);
			}

			$account = Account::createFromPayload($account);

			$followers = $this->finder->getAccountFollowers($accountId);
			$following = $this->finder->getAccountFollowing($accountId);
			$groups = $this->finder->getAccountGroups($accountId);

			$account = $account
				->withFollowers($followers)
				->withFollowing($following)
				->withGroups($groups);


			return $this->getResponseBody(
				'Account Profile',
				$account->toArray()
			);

		} catch (Exception $e) {
			throw new SomethingWentWrongException();
		}

	}

	/**
	 * @param string|null $country
	 * @param string|null $groupId
	 *
	 * @return array
	 * @throws SomethingWentWrongException
	 */
	public function searchAccounts(string $country = null, string $groupId = null): array
	{
		try {

			$accounts = $this->finder->searchAccounts(
				$country,
				$groupId
			);

			$result = [
				'items' => $accounts,
				'filtersAvailable' => [
					'countries' => $this->finder->getAllCountriesForFilter(),
					'groups' => $this->finder->getAllGroupsForFilter()
				],
				'filterUsed' => [
					'country' => $country,
					'group' => $groupId
				]
			];


			return $this->getResponseBody(
				'Search Accounts',
				$result
			);

		} catch (Exception $e) {
			throw new SomethingWentWrongException();
		}
	}

	/**
	 * @param string $accountId
	 * @param string $followingId
	 *
	 * @return array
	 * @throws AccountDoesNotExistException
	 * @throws AccountAlreadyFollowingAccountException
	 */
	public function followAccount(string $accountId, string $followingId): array
	{
		$followingAccount = $this->finder->getAccountById($followingId);

		if ($followingAccount === null) {
			throw AccountDoesNotExistException::withAccountId($followingId);
		}

		$result = $this->projector->followAccount($accountId, $followingId);

		if ($result === false) {
			throw AccountAlreadyFollowingAccountException::withFollowingId($followingId);
		}

		$isFollowingBack = $this->finder->isFollowingAccount($followingId, $accountId);

		return $this->getResponseBody(
			'Following new user',
			[
				'followingId' => $followingId,
				'isFollowingBack' => $isFollowingBack
			]
		);

	}

	/**
	 * @param string $accountId
	 * @param string $followingId
	 *
	 * @return array
	 * @throws AccountDoesNotExistException
	 * @throws AccountDoesNotFollowAccountException
	 */
	public function unfollowAccount(string $accountId, string $followingId): array
	{
		$followingAccount = $this->finder->getAccountById($followingId);

		if ($followingAccount === null) {
			throw AccountDoesNotExistException::withAccountId($followingId);
		}

		$isFollowing = $this->finder->isFollowingAccount($accountId, $followingId);

		if (!$isFollowing) {
			throw AccountDoesNotFollowAccountException::withFollowingId($followingId);
		}

		$this->projector->unfollowAccount($accountId, $followingId);
		$isFollowingBack = $this->finder->isFollowingAccount($followingId, $accountId);

		return $this->getResponseBody(
			'Unfollowing user',
			[
				'followingId' => $followingId,
				'isFollowingBack' => $isFollowingBack
			]
		);

	}


}
