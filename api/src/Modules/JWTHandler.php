<?php

declare(strict_types=1);

namespace SocialNetwork\Modules;

use Exception;
use Firebase\JWT\JWT;
use Ramsey\Uuid\Uuid;

final class JWTHandler
{

	/**
	 * @param array $data
	 *
	 * @return string
	 * @throws Exception
	 */
	public static function issueToken(array $data): string
	{
		$tokenId = Uuid::uuid4()->toString();
		$issuedAt = time();
		$notBefore = $issuedAt;
		$expire = $notBefore + JWT_DURATION;            // Adding 1 hour
		$serverName = APPLICATION_NAME;

		$jwtSecretKey = base64_decode(JWT_KEY);

		/*
		 * Create the token as an array
		 */
		$data = [
			'iat' => $issuedAt,
			'jti' => $tokenId,
			'iss' => $serverName,
			'nbf' => $notBefore,
			'exp' => $expire,
			'data' => $data
		];

		return JWT::encode($data, $jwtSecretKey, 'HS512');

	}

	/**
	 * @param string $token
	 *
	 * @return array|
	 * null
	 */
	public static function decodeToken(string $token): ?array
	{
		try {
			$jwtSecretKey = base64_decode(JWT_KEY);

			//if the decode fail will throw the exception, meaning that this token is expired.
			$jwt = (array)JWT::decode($token, $jwtSecretKey, ['HS512']);
			$jwt['data'] = (array)$jwt['data'];

			return $jwt;

		} catch (Exception $exception) {
			return null;
		}

	}

}
