<?php

declare(strict_types=1);

require __DIR__ . '/config/config.php';

return [
	'dbname' => DB_NAME,
	'user' => DB_USER,
	'password' => DB_PASS,
	'host' => DB_HOST,
	'driver' => 'pdo_mysql',
];
