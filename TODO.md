# SETUP
- [X] Makefile with start project
- [X] Setup MariaDB container  
- [X] Setup API project container
- [X] Setup Interface container

# PRE-REQUIREMENTS
- [X] Modeling Database
- [X] Doctrine Migrations 
- [X] Bootstrap api
- [X] Bootstrap client 
 
# FEATURES
- [X] Basic Routing
- [X] Basic Producer
- [X] Finder
- [X] Projector
- [X] POST Login
- [X] POST Account
- [X] POST Relationship
- [X] DELETE Relationship
- [X] GET Groups
- [X] GET Group (by id) 
- [X] POST Group Relationship

## EXTRAS
- [X] README.md with start project doc
  
## IMPROVEMENTS
- Better auto-wire with the controllers and route because of Symfony and League Route
- Adding payload examples for JSON Schema Validation
- An ASYNC Layer 
- Better Exception handling for JSON Validation
- Implement Refresh Token
- Pagination with lazy load
- Split Finders and Projectors per Domain
