SHELL := $(shell which bash)

COLOR_RESET=\033[0m
COLOR_INFO=\033[33;5m
COLOR_COMMENT=\033[32;5m
COLOR_ERROR=\033[31;5m

.PHONY: setup
setup:
	./scripts/env_checker.sh && \
	./scripts/dependencies.sh

.PHONY: up
up:
	docker-compose --file database/docker-compose.yml up -d && \
	docker-compose --file api/docker-compose.yml up -d && \
	docker-compose --file client/docker-compose.yml up -d

.PHONY: down
down:
	docker network disconnect -f default_network client-nginx && \
	docker network disconnect -f default_network api-nginx && \
	docker network disconnect -f default_network api-php && \
	docker network disconnect -f default_network mariadb && \
	docker-compose --file database/docker-compose.yml down && \
	docker-compose --file api/docker-compose.yml down && \
	docker-compose --file client/docker-compose.yml down

.PHONY: tests
tests:
	./scripts/execute_tests.sh

.PHONY: dependencies
dependencies:
	./scripts/dependencies.sh

.PHONY: migrations
migrations:
	./scripts/migrations.sh
