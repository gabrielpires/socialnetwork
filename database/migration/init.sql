
CREATE TABLE IF NOT EXISTS socialnetwork.migration_versions (
	`version` varchar(14) NOT NULL,
	executed_at timestamp NOT NULL,
	CONSTRAINT migration_versions_pkey PRIMARY KEY (version)
);
