# Techie Social Network - PHP Demo Application
**Description:** This application a demonstration of php knowledge 
 **Author:** Gabriel Pires

# Stack

## Basic
- PHP v7.3
- MariaDB
- Docker and Docker Compose v3.5

## Libs
- doctrine/migrations
- vlucas/phpdotenv
- symfony/dependency-injection
- ramsey/uuid
- league/route
- guzzlehttp/guzzle
- doctrine/dbal
- monolog/monolog
- zendframework/zend-diactoros
- zendframework/zend-httphandlerrunner
- firebase/php-jwt
- opis/json-schema
- phpunit/phpunit


# Containers

**This application uses 4 containers to run**

## 1 - MariaDB as Database (mariadb)

    Exposed PORT: 3306
    Mapped PORT to HOST: 4001
    user: socialnetwork
    pass: 123456
    database: socialnetwork

## 2 - Client NGINX Server (client-gninx) 

    http://localhost:8000

## 3 - API NGINX Server (api-gninx) 

    http://localhost:8080

## 4 - API PHP-FPM Server (api-php)

    Exposed PORT: 9000

### Routes Available

#### ACCOUNT

**Account Auth | Method: POST**

    http://localhost:8080/account/auth

> Fields: *login and password*    
    

**Account Logged Profile | Method: GET**

    http://localhost:8080/account/profile

> Header: *Authorization: Bearer <ACCESS_TOKEN>*    


**Account Members Profile | Method: GET**

    http://localhost:8080/account/profile/{accountId}

> Header: *Authorization: Bearer <ACCESS_TOKEN>*   
> Url Wild Card: accountId  


**Account Search Profiles | Method: GET**

    http://localhost:8080/account/search?groupId={uuid:groupId}&country={string:country}

> Header: Authorization: Bearer <ACCESS_TOKEN>
> Url Parameters: groupId and country  


**Account Follow Profile | Method: POST**

    http://localhost:8080/account/follow

> Header: Authorization: Bearer <ACCESS_TOKEN>
> Fields: *followingId*    


**Account Unfollow Profile | Method: DELETE**

    http://localhost:8080/account/follow

> Header: Authorization: Bearer <ACCESS_TOKEN>
> Fields: *followingId*    

#### GROUPS

Groups Get All | Method: GET

    http://localhost:8080/group/all
    

Group Profile Page | Method: GET

    http://localhost:8080/group/page/{groupId}

> Header: *Authorization: Bearer <ACCESS_TOKEN>*       
> Url Wild Card: accountId  


Follow Group Profile | Method: POST

    http://localhost:8080/group/follow

> Header: Authorization: Bearer <ACCESS_TOKEN>
> Fields: *groupId*    


Unfollow Group Profile | Method: DELETE

    http://localhost:8080/group/follow

> Header: Authorization: Bearer <ACCESS_TOKEN>
> Fields: *groupId*    


# Installation

## Pre-Requirements

 - Git
 - Docker
 - Docker Compose
 - PHP Composer

## 1 - Repository
    
    git clone git@bitbucket.org:gabrielpires/socialnetwork.git socialnetwork

## 2 - SETUP
> Copying .env file and composer dependencies installation
    
    make setup   

## 3 - Docker UP!

> All the development was made using **Docker** and **Docker Compose**.

    make up

## 4 - Database Migration
> 1 - At the first run of the containers, the database may take a few seconds extra to run get up, so if u ran the migration and failed, wait a few seconds and run again.
       
    make migrations

# Done
Just access the application and see it working
[http://localhost:8000/](http://localhost:8000/)
    
    account: account.demo | password: 123456

# Extras

## Tests
    make tests
    
## New Dependencies (if required)
    make dependencies
    
## Extra migrations? 
    make migrations      
    
## Done with the work? 
    make down          
          
# POSSIBLE IMPROVEMENTS
- Better auto-wire with the controllers and route because of Symfony and League Route
- Adding payload examples for JSON Schema Validation
- An ASYNC Layer 
- Better Exception handling for JSON Validation
- Implement Refresh Token
- Pagination with lazy load
- Split Finders and Projectors per Domain
      

